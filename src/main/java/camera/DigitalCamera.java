/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package camera;

/**
 *
 * @author iuabd
 */
public abstract class DigitalCamera {
    
    //attribute(s)s
    protected String make;
    protected String model;
    protected double megapixels;
    protected int internalMemory;
    protected int externalMemory;
    
    //constructor(s)
    public DigitalCamera(String make, String model, double megapixels, int internalMemory, int externalMemory) {
        this.make = make;
        this.model = model;
        this.megapixels = megapixels;
        this.internalMemory = internalMemory;
        this.externalMemory = externalMemory;
    }
    
    //getter(s)
    public abstract String getMake();
    public abstract String getModel();
    public abstract double getMegapixels();
    public abstract int getInternalMemory();
    public abstract int getExternalMemory();
    
    //method(s)
    public abstract String describeCamera();
    
    
}
