/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package camera;

/**
 *
 * @author iuabd
 */
public class DigitalCameraSimulation {
    
    public static void main(String[]args){
        
        DigitalCamera phoneCamera = new PhoneCamera("Apple","iPhone",6, 64, 0);
        DigitalCamera posCamera = new PointAndShootCamera("Canon","Powershot A590",8, 0, 16);
        
        System.out.println("Camera #1" + phoneCamera.describeCamera());
        System.out.println("Camera #2" + posCamera.describeCamera());
        
    }
}
