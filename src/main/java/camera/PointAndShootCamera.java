/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package camera;

/**
 *
 * @author iuabd
 */
public class PointAndShootCamera extends DigitalCamera {
    
//constructor(s)
    public PointAndShootCamera(String make, String model, double megapixels, int internalMemory, int externalMemory){
        super(make, model, megapixels, internalMemory, externalMemory);
    }

    //getter(s)
    @Override
    public String getMake() {
        return super.make;
    }
    public String getModel(){
        return super.model;
    }
    public double getMegapixels(){
        return super.megapixels;
    }
    public int getInternalMemory(){
        return super.internalMemory;
    }
    public int getExternalMemory(){
        return super.externalMemory;
    }
    
    //method(s)
    @Override
    public String describeCamera(){
        return "\nWas made by: "  + this.getMake() +
                "\nModel: " + this.getModel() +
                "\nMegapixels: " + this.getMegapixels() +
                "\nInternal Memory: " + this.getInternalMemory() + "GB" +
                "\nExternal Memory: " + this.getExternalMemory() + "GB";
    }    
}
